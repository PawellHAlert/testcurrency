package com.example.psc.currency.Common

import com.example.psc.currency.Models.CbCurrency
import com.example.psc.currency.Models.DALModels.LocalCurrency

val CURRENCY_RATE = "CURRENCY_RATE"
val CURRENCY_CODE = "CURRENCY_CODE"

//TODO replace by modelMapper or sumthing like that
fun mapToCbCurrency(localCurrency: LocalCurrency): CbCurrency {
    var cbCurrency = CbCurrency()
    cbCurrency.Vcode = localCurrency.Vcode
    cbCurrency.Vname = localCurrency.Vname
    cbCurrency.VEngname = localCurrency.VEngname
    cbCurrency.Vnom = localCurrency.Vnom
    cbCurrency.VcommonCode = localCurrency.VcommonCode
    cbCurrency.VnumCode = localCurrency.VnumCode
    cbCurrency.VcharCode = localCurrency.VcharCode
    return  cbCurrency
}

fun mapFromCbCurrency(cbCurrency: CbCurrency): LocalCurrency {

    return LocalCurrency(
            cbCurrency.Vcode,
            cbCurrency.Vname,
            cbCurrency.VEngname,
            cbCurrency.Vnom,
            cbCurrency.VcommonCode,
            cbCurrency.VnumCode,
            cbCurrency.VcharCode
    )
}