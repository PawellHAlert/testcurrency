package com.example.psc.currency.Common.DI



import com.example.psc.currency.CurrencyApplication
import com.example.psc.currency.activities.Currency.CurrencyActivity
import com.example.psc.currency.activities.Main.MainActivity

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RepoModule::class,AppModule::class,DatabaseModule::class])
interface AppComponent{
    fun inject(target:CurrencyApplication)
    fun inject(target: MainActivity)
    fun inject(target: CurrencyActivity)
}

