package com.example.psc.currency.Common.DI

import android.arch.persistence.room.Room
import android.content.Context
import com.example.psc.currency.Repositories.Database.RatesDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private val applicationContext: Context) {
    val ratesDatabase = Room.databaseBuilder(applicationContext, RatesDatabase::class.java, "rates-database").build()
    @Provides
    @Singleton
    fun getDatabase():RatesDatabase {
        return ratesDatabase
    }
}
