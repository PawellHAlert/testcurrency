package com.example.psc.currency.Common.DI

import com.example.psc.currency.Repositories.CBCurrencyRepoImpl
import com.example.psc.currency.Repositories.CbQuotesRepoImpl
import com.example.psc.currency.Repositories.Interfaces.CBCurrencyRepo
import com.example.psc.currency.Repositories.Interfaces.CbQuotesRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {
    @Provides
    @Singleton
    fun getCurrencyRepo():CBCurrencyRepo{
        return CBCurrencyRepoImpl()
    }
    @Provides
    @Singleton
    fun getQuotesRepo():CbQuotesRepo{
        return CbQuotesRepoImpl()
    }
}