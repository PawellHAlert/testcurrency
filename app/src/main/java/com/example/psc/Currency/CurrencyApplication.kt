package com.example.psc.currency

import android.app.Application
import com.example.psc.currency.Common.DI.*
import com.example.psc.currency.Repositories.Database.RatesDatabase
import javax.inject.Inject

//import com.example.psc.currency.Common.DI.AppModule


class CurrencyApplication : Application(){
    @Inject
    lateinit var ratesDatabase: RatesDatabase
     val currencyComponent : AppComponent by lazy {
      DaggerAppComponent.builder()
              .databaseModule(DatabaseModule(this))
              .appModule(AppModule(this))
                .build()
    }


    override fun onCreate() {
        super.onCreate()
       currencyComponent.inject(this)
    }

}