package com.example.psc.currency.Models
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import java.lang.reflect.Constructor

//TODO make in order this shit with strange annotation

class CbCurrency  {
    @JacksonXmlProperty
    var Vcode: String = ""
    @JacksonXmlProperty
    var Vname: String =""
    @JacksonXmlProperty
    var VEngname: String =""
    @JacksonXmlProperty
    var Vnom: Int =0
    @JacksonXmlProperty
    var VcommonCode: String=""
    @JacksonXmlProperty
    var VnumCode: String=""
    @JsonProperty
    var VcharCode: String=""
}
