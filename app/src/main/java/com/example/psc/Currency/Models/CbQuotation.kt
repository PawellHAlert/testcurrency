package com.example.psc.currency.Models

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import java.util.*

class CbQuotation {
    @JacksonXmlProperty
    var CursDate: Date = Date()
    @JacksonXmlProperty
    var Vcode: String = ""
    @JacksonXmlProperty
    var Vnom: Double = .0
    @JacksonXmlProperty
    var Vcurs: Double = .0
}