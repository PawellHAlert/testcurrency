package com.example.psc.currency.Models.Converters

import android.arch.persistence.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class DateTypeConverter {
    private val  df =  SimpleDateFormat("yyyy-MM-dd")
    @TypeConverter
    fun fromDate(date: Date):String{
      return  df.format(date)
    }
    @TypeConverter
    fun toDate(date:String):Date{
        return df.parse(date)
    }
}