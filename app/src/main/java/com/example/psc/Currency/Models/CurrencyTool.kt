package com.example.psc.currency.Models

import java.util.*


data class CurrencyTool(
        var SecId: String = "",
        var BoardId: String = "",
        var ShortName: String = "",
        var LotSize: Int = 0,
        var SettledDate: Date = Date(),
        var Decimals: Int = 0,
        var FaceValue: Double = .0,
        var MarketCOde: String = "",
        var MinStep: Double = .0,
        var PrevDate: Date = Date(),
        var SecName: String = "",
        var Remarks: String? = null,
        var Status: String = "",
        var FaceUnit: String = "",
        var PrevPrice: Double? = null,
        var PrevWPrice: Double? = null,
        var CurrencyId: String = "",
        var LatName: String = ""
)