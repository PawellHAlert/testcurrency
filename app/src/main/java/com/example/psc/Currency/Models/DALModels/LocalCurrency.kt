package com.example.psc.currency.Models.DALModels

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.time.Instant
import java.util.*


@Entity
class LocalCurrency (

    @ColumnInfo(name = "Vcode")
    var Vcode: String = "",
    @ColumnInfo(name = "Vname")
    var Vname: String = "",
    @ColumnInfo(name = "VEngname")
    var VEngname: String = "",
    @ColumnInfo(name = "Vnom")
    var Vnom: Int = 0,
    @ColumnInfo(name = "VcommonCode")
    var VcommonCode: String = "",
    @ColumnInfo(name = "VnumCode")
    var VnumCode: String = "",
    @ColumnInfo(name = "VcharCode")
    var VcharCode: String = ""

){
    @PrimaryKey
    var uid: Int = 0
    @ColumnInfo(name ="DownloadDate")
    var DownloadDate:Date = GregorianCalendar(1970,Calendar.JANUARY,1).time
}