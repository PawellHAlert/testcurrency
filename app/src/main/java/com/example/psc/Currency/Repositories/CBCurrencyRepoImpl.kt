package com.example.psc.currency.Repositories

import com.ctc.wstx.stax.WstxInputFactory
import com.ctc.wstx.stax.WstxOutputFactory
import com.example.psc.currency.Models.CbCurrency
import com.example.psc.currency.Repositories.Interfaces.CBCurrencyRepo
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE
import javax.xml.stream.XMLInputFactory


class CBCurrencyRepoImpl : CBCurrencyRepo  {
   override fun getCurrencies():Collection<CbCurrency> {
        val methodName = "EnumValutesXML"
        val soapObject = SoapObject("http://web.cbr.ru/", methodName)
        soapObject.addProperty("Seld", false)
        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER12)
        envelope.setOutputSoapObject(soapObject)
        envelope.dotNet = true
        val httpTransportSE = HttpTransportSE("http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL")
        httpTransportSE.debug = true
        try {
            httpTransportSE.call(methodName, envelope)
            //   val responseObject = envelope.bodyIn as SoapObject
            val inputFactory = WstxInputFactory()
            inputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false)
            val  xmlMapper = XmlMapper(inputFactory, WstxOutputFactory())
            xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            val result = xmlMapper.readValue(httpTransportSE.responseDump,
                    Envelope::class.java)
            return result?.Body?.EnumValutesXMLResponse
                    ?.EnumValutesXMLResult?.ValuteData
                    ?: listOf()

        } catch (ex: Exception) {
            ex.printStackTrace()
            throw ex
        }
    }

    class XMLResult {
        @JacksonXmlProperty
        val ValuteData: Collection<CbCurrency>? = null
    }

    class XMLResponse {
        @JacksonXmlProperty
        val EnumValutesXMLResult: XMLResult? = null

    }

    class SoapBody {
        @JacksonXmlProperty
        val EnumValutesXMLResponse: XMLResponse? = null
    }

    class Envelope {

        @JsonProperty("soap:Body")
        val Body: SoapBody? = null
    }
}