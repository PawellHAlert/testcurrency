package com.example.psc.currency.Repositories


import com.ctc.wstx.stax.WstxInputFactory
import com.ctc.wstx.stax.WstxOutputFactory
import com.example.psc.currency.Models.CbQuotation
import com.example.psc.currency.Repositories.Interfaces.CbQuotesRepo
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.stream.XMLInputFactory

class CbQuotesRepoImpl : CbQuotesRepo {

    override fun getQuotations(dateFrom: Date, dateTo: Date, vcode:String):Collection<CbQuotation> {
        val df =  SimpleDateFormat("yyyy-MM-dd")
        val methodName = "GetCursDynamicXML"
        val soapObject = SoapObject("http://web.cbr.ru/", methodName)
        soapObject.addProperty("FromDate", df.format(dateFrom))
        soapObject.addProperty("ToDate", df.format(dateTo))
        soapObject.addProperty("ValutaCode",vcode)
        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER12)
        envelope.setOutputSoapObject(soapObject)
        envelope.dotNet = true
        val httpTransportSE = HttpTransportSE("http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL")
        httpTransportSE.debug = true
        try {
            httpTransportSE.call(methodName, envelope)
            val inputFactory = WstxInputFactory()
            inputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false)
            val xmlMapper = XmlMapper(inputFactory, WstxOutputFactory())
            xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            val result = xmlMapper.readValue(httpTransportSE.responseDump,
                    Envelope::class.java)
            return result?.Body?.GetCursDynamicXMLResponse
                    ?.GetCursDynamicXMLResult?.ValuteData
                    ?: listOf()

        } catch (ex: Exception) {
            ex.printStackTrace()
            throw ex
        }
    }

    class XMLResult {
        @JacksonXmlProperty
        val ValuteData: Collection<CbQuotation>? = null
    }

    class XMLResponse {
        @JacksonXmlProperty
        val GetCursDynamicXMLResult: XMLResult? = null

    }

    class SoapBody {
        @JacksonXmlProperty
        val GetCursDynamicXMLResponse: XMLResponse? = null
    }

    class Envelope {

        @JsonProperty("soap:Body")
        val Body: SoapBody? = null
    }

}