package com.example.psc.currency.Repositories

import com.example.psc.currency.Models.CurrencyTool
import org.json.JSONObject
import java.net.URL
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties
import java.text.SimpleDateFormat
import java.util.*

class CurencyToolsRepository {
    fun GetAllTools(): Iterable<CurrencyTool>  {
            val tools = mutableListOf<CurrencyTool>()
            var url = URL("http://iss.moex.com/iss/engines/currency/markets/selt/securities.json")
            val json = url.readText()
            val jsonObject = JSONObject(json)
            val data = jsonObject.getJSONObject("securities").getJSONArray("data")
            val columns = jsonObject.getJSONObject("securities").getJSONArray("columns")
            for(i in 0..(data.length()-1)){
                var tool = CurrencyTool()
                for(j in 0..(columns.length()-1)){
                    val columnName = columns.getString(j)
                  for(proper in tool::class.memberProperties){
                      if(proper.name.equals(columnName,true) && proper is KMutableProperty<*> ) {
                         when(proper.returnType.classifier){
                              Date::class -> {
                                 val format = SimpleDateFormat("YYYY-MM-DD")
                                  proper.setter.call(tool,format.parse(data.getJSONArray(i)[j]?.toString()))
                             }
                              Int::class -> {

                                  proper.setter.call(tool,(data.getJSONArray(i)[j]?.toString())?.toIntOrNull())
                             }
                             Double::class -> {

                                 proper.setter.call(tool,(data.getJSONArray(i)[j]?.toString())?.toDoubleOrNull())
                             }
                             else-> proper.setter.call(tool,data.getJSONArray(i)[j]?.toString())

                         }
                      }
                  }
                }
                tools.add(tool)
            }


        return tools
        }
    }
