package com.example.psc.currency.Repositories.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.example.psc.currency.Models.Converters.DateTypeConverter
import com.example.psc.currency.Models.DALModels.LocalCurrency


@TypeConverters(DateTypeConverter::class)
@Database(entities = [LocalCurrency::class], version = 2)
 abstract class RatesDatabase : RoomDatabase() {
    abstract fun  getCurrencyDao(): CurrencyDao

}