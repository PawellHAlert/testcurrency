package com.example.psc.currency.Repositories.Interfaces

import com.example.psc.currency.Models.CbCurrency

interface CBCurrencyRepo  {
   fun getCurrencies():Collection<CbCurrency>
}