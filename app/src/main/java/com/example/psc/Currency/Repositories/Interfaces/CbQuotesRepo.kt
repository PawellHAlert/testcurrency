package com.example.psc.currency.Repositories.Interfaces

import com.example.psc.currency.Models.CbQuotation
import java.util.*

interface CbQuotesRepo {
    fun getQuotations(dateFrom: Date, dateTo: Date, vcode: String): Collection<CbQuotation>
}