package com.example.psc.currency.Repositories.Interfaces

import com.example.psc.currency.Models.CbCurrency
import com.example.psc.currency.Models.DALModels.LocalCurrency
import java.util.*

interface LocalCurrencyRepo {
    fun getGurrencies(): Iterable<LocalCurrency>
    fun addCurrency(currencies: Iterable<LocalCurrency>)
    fun deleteCurrency(vcodes :List<String>)
}