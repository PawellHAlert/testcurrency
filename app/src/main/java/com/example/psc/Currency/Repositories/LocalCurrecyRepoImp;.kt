package com.example.psc.currency.Repositories


import com.example.psc.currency.Models.DALModels.LocalCurrency
import com.example.psc.currency.Repositories.Database.RatesDatabase
import com.example.psc.currency.Repositories.Interfaces.LocalCurrencyRepo
import javax.inject.Inject

class LocalCurrecyRepoImp @Inject constructor(ratesDatabase: RatesDatabase) : LocalCurrencyRepo {
     private val _ratesDatabase: RatesDatabase = ratesDatabase

    override fun getGurrencies(): Iterable<LocalCurrency> {
       return _ratesDatabase.getCurrencyDao().getAll()
    }

    override fun addCurrency(currencies: Iterable<LocalCurrency>) {
       _ratesDatabase.getCurrencyDao().insertAll(currencies)
    }

    override fun deleteCurrency(vcodes :List<String>) {
      _ratesDatabase.getCurrencyDao().delete(vcodes)
    }
}