package com.example.psc.currency.activities.Currency


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.psc.currency.Common.CURRENCY_CODE
import com.example.psc.currency.Common.CURRENCY_RATE
import com.example.psc.currency.CurrencyApplication
import com.example.psc.currency.R
import com.example.psc.currency.Repositories.Interfaces.CbQuotesRepo
import kotlinx.android.synthetic.main.activity_currency.*
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.thread

class CurrencyActivity : AppCompatActivity() {
    @Inject
    lateinit var repo: CbQuotesRepo
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as CurrencyApplication).currencyComponent.inject(this)
        setContentView(R.layout.activity_currency)
        val vcommonCode = intent.getStringExtra(CURRENCY_RATE)
        val vcharCode = intent.getStringExtra(CURRENCY_CODE)
        vcommonCode?.let{
            thread {

                val today = Date()
                val calendar = Calendar.getInstance()
                calendar.add(Calendar.DAY_OF_MONTH, -1)
                val yesterday = calendar.time
                try {
                    val currRate = repo.getQuotations(yesterday, today,vcommonCode.trim()).sortedBy { it.CursDate }.lastOrNull()
                    if(currRate!=null) {
                        runOnUiThread {
                            currencyDate.text = currRate.CursDate.toString()
                            rate.text = "${currRate.Vcurs.toString()} RUB"
                            currncy_symbol.text = vcharCode
                            nom.text = "${currRate.Vnom} ${vcharCode} ="
                        }
                    }
                    else{
                        runOnUiThread {
                        currencyDate.text = ""
                        rate.text ="No data"
                        currncy_symbol.text = vcharCode
                        }
                    }
                } catch (ex: Exception) {
                    runOnUiThread {
                        Toast.makeText(this, ex.message, Toast.LENGTH_LONG).show()
                    }
                }

            }
        }

    }
}
