package com.example.psc.currency.activities.Main


import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.psc.currency.R
import com.example.psc.currency.activities.Main.adapters.ToolAdapter


import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.widget.Toast
import com.example.psc.currency.CurrencyApplication
import com.example.psc.currency.Repositories.CBCurrencyRepoImpl
import com.example.psc.currency.Repositories.Interfaces.CBCurrencyRepo
import javax.inject.Inject


class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var repo : CBCurrencyRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as CurrencyApplication).currencyComponent.inject(this)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        tools_recycler_view.layoutManager = LinearLayoutManager(this)
        val toolAdapter = ToolAdapter( this)
        tools_recycler_view.adapter= toolAdapter
        var filterString = ""
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also {
                filterString = it
            }
        }

        thread {
            try {

                val tools = repo.getCurrencies()

                runOnUiThread {
                    toolAdapter.items = tools.toList()
                    toolAdapter.filter.filter(filterString)

                }
            }catch(ex:Exception){
                runOnUiThread {
                    Toast.makeText(this, ex.message, Toast.LENGTH_LONG).show()
                    Log.e("error","${ex.message} ${ex.stackTrace}")
                }
            }

        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.menu_main, menu)

        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.maxWidth=Integer.MAX_VALUE


        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {

                (tools_recycler_view.adapter as ToolAdapter).filter.filter(query)
                return false
            }

           override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
               (tools_recycler_view.adapter as ToolAdapter).filter.filter(query)
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
