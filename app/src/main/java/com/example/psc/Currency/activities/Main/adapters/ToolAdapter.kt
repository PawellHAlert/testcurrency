package com.example.psc.currency.activities.Main.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.example.psc.currency.Common.CURRENCY_CODE
import com.example.psc.currency.Common.CURRENCY_RATE
import com.example.psc.currency.Models.CbCurrency
import com.example.psc.currency.R
import com.example.psc.currency.activities.Currency.CurrencyActivity
import kotlinx.android.synthetic.main.tool_list_item.view.*


class ToolAdapter(private val context: Context) : Adapter<ViewHolder>(), Filterable {

    private var currencyFiltered = listOf<CbCurrency>()
    var items = listOf<CbCurrency>()

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                var filterResult = FilterResults()
                if (charSequence.isEmpty()) {
                    filterResult.values = items

                } else {
                    filterResult.values = (items.filter { it.VEngname.contains(charSequence, true) }).toList()

                }

                return filterResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                if (p1 != null) {
                    currencyFiltered = p1.values as List<CbCurrency>
                } else {
                    currencyFiltered = listOf()
                }
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view  = LayoutInflater.from(context).inflate(R.layout.tool_list_item, parent, false)
        var viewHolder = MyViewHolder(view)
        view.setOnClickListener {
            val intent = Intent(context,CurrencyActivity::class.java)
            intent.putExtra(CURRENCY_RATE,currencyFiltered[viewHolder.adapterPosition].VcommonCode)
            intent.putExtra(CURRENCY_CODE,currencyFiltered[viewHolder.adapterPosition].VcharCode)

            context.startActivity(intent)
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return currencyFiltered.count()
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        if (holder is MyViewHolder) {
            holder.toolType?.text = currencyFiltered.get(position).VEngname
        }
    }

    class MyViewHolder(view: View) : ViewHolder(view) {
        val toolType = view.rv_tool
    }
}